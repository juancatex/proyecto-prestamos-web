<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->tipouser = 2;
        $user->name = 'user'; 
        $user->password = bcrypt('secret');
        $user->save();

        $user = new User();
        $user->tipouser = 3;
        $user->name = 'personal'; 
        $user->password = bcrypt('personal');
        $user->save();

        $user = new User();
        $user->tipouser = 1;
        $user->name = 'admin';  //es el admin
        $user->password = bcrypt('secret');
        $user->save();

        $user = new User(); 
        $user->tipouser = 4;
        $user->name = 'mcriales'; 
        $user->password = bcrypt('mcriales');
        $user->save();
    }
}
