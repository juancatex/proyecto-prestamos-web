@extends('layouts.reg')

@section('content') 
 


<div class="container">
    <div class="row">
        <div class="col-lg-6 col-md-8 col-sm-10 ml-auto mr-auto"> 
            <div class="card card-login text-center card-hidden">
                <div class="card-header " style="margin-bottom: 0px;">
                    <div class="card-avatar">
                        <img class="img" style="width: 30%;" src="img/icon.png">
                    </div>
                </div> 
                 
                <h3 class="card-title" style="margin: 0px;">Registro de prestamo no disponible</h3>
                
                    @if($respuesta=="nohora") 
                        
                        <h4>Horario de atención  <span class="card-title" style="margin: 0px;">{{$horainicio}} - {{$horafin}}</span> </h4>
                        <h3>Lunes a viernes</h3>
                 
                    @endif
                    
                    <div class="card-footer justify-content-center"> 
                    <a class="btn btn-success btn-lg btn-block" href="http://www.ascinalss.org/ascinalss/index.html">Volver</a>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
 