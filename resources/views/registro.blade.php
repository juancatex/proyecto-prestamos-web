@extends('layouts.reg')

@section('content') 
 


<div class="container">
    <div class="row">
        <div class="col-lg-6 col-md-8 col-sm-10 ml-auto mr-auto"> 
            <div class="card card-login text-center card-hidden">
                <div class="card-header " style="margin-bottom: 0px;">
                    <div class="card-avatar">
                        <img class="img" style="width: 30%;" src="img/icon.png">
                    </div>
                </div>
                <h3 class="card-title" style="margin: 0px;"> Registro de Datos<br>Prestamo de Emergencia
                </h3> 
                          @if ($errors->any())
                            <div class="alert alert-danger" style="border-radius: 11px;border-radius: 11px;margin:15px 15px;"
                                role="alert">
                                @foreach ($errors->all() as $error)
                                <p style="margin: 0;padding: 0;">{{ $error }}</p>
                                @endforeach
                            </div>
                            @endif  

          <form action="{{ route('registroapi') }}" method="POST"  enctype="multipart/form-data"> @csrf
          <div class="card-body row">
          
        
                     <span class="bmd-form-group col-md-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">perm_identity</i>
                                        </span>
                                    </div>
                                    <label for="name" class="bmd-label-floating"
                                        style="margin: 6px 0 0 55px;">Nombre Completo:</label> 
                                    <input type="text" id="nombres" name="nombres"class="form-control" required autofocus/>
                                </div>
                            </div>
                        </span>

                        
                  <span class="bmd-form-group col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">pin</i>
                                        </span>
                                    </div>
                                    <label for="name" class="bmd-label-floating"
                                        style="margin: 6px 0 0 55px;">CI:</label>  
                                      <input type="number" id="ci" name="ci" class="form-control" required/>
                                </div>
                            </div>
                        </span>
              <span class="bmd-form-group col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">money</i>
                                        </span>
                                    </div>
                                    <label for="name" class="bmd-label-floating"
                                        style="margin: 6px 0 0 55px;">Liquido Pagable:</label>   
                                      <input type="number" id="liquido" name="liquido" class="form-control"  required />
                                </div>
                            </div>
                        </span>
  
                        <span class="bmd-form-group col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">paid</i>
                                        </span>
                                    </div>
                                    <label for="name" class="bmd-label-floating"
                                        style="margin: 6px 0 0 55px;">Monto Solicitado:</label>    
                                      <input type="number" id="solicitado" name="solicitado" class="form-control "   required />
                                </div>
                            </div>
                        </span>

                      
               
                        <span class="bmd-form-group col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">credit_score</i>
                                        </span>
                                    </div>
                                    <label for="name" class="bmd-label-floating"
                                        style="margin: 6px 0 0 55px;">N° Cuenta Banco Union:</label>     
                                      <input type="text" id="cuenta" name="cuenta" class="form-control " required/>
                                </div>
                            </div>
                        </span>

                        <span class="bmd-form-group col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">phone</i>
                                        </span>
                                    </div>
                                    <label for="name" class="bmd-label-floating"
                                        style="margin: 6px 0 0 55px;">N° Telefono:</label>     
                                        <input type="number" id="telefono" name="telefono" class="form-control" required/>
                                </div>
                            </div>
                        </span>
                        <span class="bmd-form-group col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">timer</i>
                                        </span>
                                    </div>
                                    
                                     
                                    <div class=" ">
                                    <select name="plazo" class="btn btn-primary btn-round " title="Single Select">
                                    <option value="1" selected="selected">Un Mes</option>
                                        <option value="2">Dos Meses</option>
                                        <option value="3">Tres Meses</option>
                                        <option value="4">Cuatro Meses</option>
                                        <option value="5">Cinco Meses</option>
                                        <option value="6">Seis Meses</option>
                                        <option value="7">Siete Meses</option>
                                        <option value="8">Ocho Meses</option>
                                        <option value="9">Nueve Meses</option>
                                        <option value="10">Dies Meses</option>
                                    </select>
                                    </div>
                                </div>
                            </div>
                        </span> 

                <span class="bmd-form-group col-md-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="material-icons">file_present</i>
                                        </span>
                                    </div>
                                    <label for="name" 
                                        style="margin: 17px 0 0 0px;">Documentos:</label>  
                                     
                                    <div class="col-md-12 ml-3 form-control">
                                    <input type="file" name="documentos[]" accept="image/*,.pdf" multiple style="opacity: 1;    z-index: 4;" required> 
                                    </div>
                                </div>
                            </div>
                        </span>
                
                
 
                </div>
                    <div class="card-footer justify-content-center"> 
                            <button class="btn btn-success btn-lg btn-block" type="submit">Solicitar prestamo</button>
                    </div>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection
 