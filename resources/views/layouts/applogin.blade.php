<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">  
    <meta name="csrf-token" content="{{ csrf_token() }}"> 
    <title> Ascinalss </title> 
    <link rel="apple-touch-icon" sizes="76x76" href="img/icon.png">
    <link rel="icon" type="image/png" href="img/icon.png"> 
    <link href="{{ asset('css/plantilla.css') }}" rel="stylesheet"> 
</head>
<body class="off-canvas-sidebar">
    <div id="app">
        <div class="wrapper wrapper-full-page">
            <div class="page-header login-page header-filter" filter-color="black" style="background-image: url('img/login.jpg'); background-size: cover; background-position: top center;">
                  @yield('content') 
                <footer class="footer">
                    <div class="container"> 
                    <div class="copyright ">
                        &copy; 2021, Copyright: Sistemas - Ascinalss.
                    </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>  
    <script src="{{ asset('js/app.js') }}" ></script>
    <script src="{{ asset('js/login.js') }}"></script>   
</body>
</html>
