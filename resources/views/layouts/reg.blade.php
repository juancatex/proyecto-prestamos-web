 

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">  
    <meta name="csrf-token" content="{{ csrf_token() }}"> 
    <title> Ascinalss </title> 
    <link rel="apple-touch-icon" sizes="76x76" href="img/icon.png">
    <link rel="icon" type="image/png" href="img/icon.png"> 
    <link href="{{ asset('css/plantilla.css') }}" rel="stylesheet"> 
    <style>
        .login-page .card-login .card-body .input-group .form-control {
    padding-bottom: 10px;
    margin: 5px 0 0 0;
}
.login-page .card-login .card-header {
    margin-top: -88px !important;
    margin-bottom: 20px;
}
.off-canvas-sidebar .wrapper-full-page .page-header {
    padding: 10vh 0 !important;
}
.container-login100 {
     
    background: #0250c5;
    background: -webkit-linear-gradient(bottom, #0250c5, #d43f8d);
    background: -o-linear-gradient(bottom, #0250c5, #d43f8d);
    background: -moz-linear-gradient(bottom, #0250c5, #d43f8d);
    background: linear-gradient(bottom, #0250c5, #d43f8d);
  
}
    </style>
</head>
<body class="off-canvas-sidebar">
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
    <div id="app" class="container-login100">
        <div class="wrapper wrapper-full-page">
            <div class="page-header login-page  " filter-color="black" >
                  @yield('content') 
                <footer class="footer">
                    <div class="container"> 
                    <div class="copyright ">
                        &copy; 2021, Copyright: Sistemas - Ascinalss.
                    </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>  
    <script src="{{ asset('js/app.js') }}" ></script>
    <script src="{{ asset('js/login.js') }}"></script>  
     
</body>
</html>