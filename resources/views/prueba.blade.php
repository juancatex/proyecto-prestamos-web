<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    
</body>
</html>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header">{{ __('Registros') }}</div>

                <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombres</th>
                            <th scope="col">CI</th>
                            <th scope="col">Liquido</th>
                            <th scope="col">Solicitado</th>
                            <th scope="col">Plazo</th>
                            <th scope="col">Cuenta</th>
                            <th scope="col">Telefono</th>
                            <th scope="col">Archivos</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($files as $file)
                        <tr>
                            <th scope="row">{{ $file->id }}</th>
                            <td style="text-align: left;">{{ $file->nombres }}</td>
                            <td style="text-align: right;">{{ $file->ci }}</td>
                            <td style="text-align: right;">{{ $file->liquido }}</td>
                            <td style="text-align: right;">{{ $file->solicitado }}</td>
                            <td style="text-align: right;">{{ $file->plazo }}</td>
                            <td style="text-align: right;">{{ $file->cuenta }}</td>
                            <td style="text-align: right;">{{ $file->telefono }}</td>
                            <td > @foreach($file->archivos as $archivo )
                                <a target="_blank" href="storage/pdf/{{ $file->id }}/{{ $archivo->nombre }}" class="btn btn-sm btn-outline-success">{{ $archivo->nombre}}</a> <br>
                                @endforeach
                            </td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                    </table>
                </div>
                   
                    
                </div>
            </div>
        </div>
    </div>
</div>
