@extends('layouts.app')
@section('usuario')  
  
<?php 
        use Illuminate\Support\Facades\DB;
        $tipo= DB::table('tipo_user')->where('tipouser',Auth::user()->tipouser)->get();  
        echo $tipo[0]->descripcion;
  ?>
@endsection
@section('content') 

<?php 
        use App\Models\Observacion;
        use App\Models\Archivo;
        use App\Models\registro; 
        $files= registro::where('estado',$activo)
        ->whereMonth('created_at',$mes)
                            ->orderby('id','asc')
                                ->get();  
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $getMonth = [];
            foreach (range(1, date("n")) as $m) { 
                $valuee=new stdClass(); 
                $valuee->nombre = $meses[$m - 1];
                $valuee->id =$m;
                $getMonth[] =$valuee;
            }

        ?>

<div class="container" style="    max-width: 1500px;">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header row"> 

                <div class="form-group col-md-6 row">
                    <label for="combomes" class="col col-md-2">Mes</label>
                    <select class="form-control col col-md-10" id="combomes" onchange="window.location.href=this.options[this.selectedIndex].value;">
                       @foreach($getMonth as $month) 
                            @if ($month->id==$mes)
                            <option  value="{{ route('fecha',['id' => $month->id, 'vista' => 'prestamos', 'activo' => $activo]) }}" selected>{{ $month->nombre }}</option>
                            @else
                            <option  value="{{ route('fecha',['id' => $month->id, 'vista' => 'prestamos', 'activo' => $activo]) }}">{{ $month->nombre }}</option>
                            @endif
                       @endforeach
                    </select>
                </div>
                
                <div class="form-group col-md-6 ">
                        @if ($activo==1)
                                <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" onclick="window.location.href=this.value"
                                value="{{ route('fecha',['id' => $mes, 'vista' => 'prestamos', 'activo' => 1]) }}" checked>
                                <label class="form-check-label" for="flexRadioDefault1">
                                Prestamos no procesados
                                </label>
                                </div> 
                                <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" onclick="window.location.href=this.value" 
                                value="{{ route('fecha',['id' => $mes, 'vista' => 'prestamos', 'activo' => 0]) }}" >
                                <label class="form-check-label" for="flexRadioDefault2">
                                Prestamos procesados
                                </label>
                                </div>
                        @else
                                <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" onclick="window.location.href=this.value"
                                value="{{ route('fecha',['id' => $mes, 'vista' => 'prestamos', 'activo' => 1]) }}" >
                                <label class="form-check-label" for="flexRadioDefault1">
                                Prestamos no procesados
                                </label>
                                </div> 
                                <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" onclick="window.location.href=this.value" 
                                value="{{ route('fecha',['id' => $mes, 'vista' => 'prestamos', 'activo' => 0]) }}" checked>
                                <label class="form-check-label" for="flexRadioDefault2">
                                Prestamos procesados
                                </label>
                                </div>
                        @endif
                </div>

                </div>
                </div>

                <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col" style="text-align: center;">#</th>
                            <th scope="col" style="text-align: center;">Nombres</th>
                            <th scope="col" style="text-align: center;">CI</th>
                            <th scope="col" style="text-align: center;">Liquido</th>
                            <th scope="col" style="text-align: center;">Solicitado</th>
                            <th scope="col" style="text-align: center;">Plazo</th>
                            <th scope="col" style="text-align: center;">Cuenta</th>
                            <th scope="col" style="text-align: center;">Telefono</th>
                            <th scope="col" style="text-align: center;">fecha</th>
                            <th scope="col" style="text-align: center;">Archivos</th> 
                            <th scope="col" style="text-align: center;">Observaciones</th>
                            <th scope="col" style="text-align: center;">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $contador=0;
                        foreach ($files as $value) {
                            $archivos=Archivo::where('idregistro',$value->id)
                                            ->get();
                            $value->archivos=$archivos;
                            $obss=Observacion::where('idregistro',$value->id)
                                                ->orderby('id','desc')
                                                ->get();
                            $value->obs=$obss;
                             $contador++;
                            echo ' <tr>
                            <th scope="row">'.$contador.' ('.$value->id.') </th>
                            <td style="text-align: left;">'.$value->nombres.'</td>
                            <td style="text-align: right;">'.$value->ci.'</td>
                            <td style="text-align: right;">'.$value->liquido.'</td>
                            <td style="text-align: right;">'.$value->solicitado.'</td>
                            <td style="text-align: right;">'.$value->plazo.'</td>
                            <td style="text-align: right;">'.$value->cuenta.'</td>
                            <td style="text-align: right;">'.$value->telefono.'</td>
                            <td style="text-align: right;">'.$value->created_at.'</td>
                            <td >';
                             foreach($value->archivos as $archivo ) {
                               echo '<a target="_blank" href="storage/pdf/'.$value->id.'/'.$archivo->nombre.'" class="btn btn-sm btn-outline-success">'.$archivo->nombre.'</a> <br>'; 
                            } 
                         echo '</td> <td >'; 
                         foreach($value->obs as $obser ) {
                            echo '-'.$obser->obs.'<br>'; 
                         }
                         echo '</td> ';
                         ?> 
                      @if ($activo==1)  
                      <td >
                         <form method="POST" action = "{{ route('update', $value->id) }}">
                           @csrf 
                             <button class="btn btn-primary btn-round" type="submit">Procesar</button>
                         </form>
 <div class="modal fade" id="Pobs-{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="Pobs">Registrar observaci贸n</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
          </div> 
                    <form method="POST" action = "{{ route('observacion', $value->id) }}"> 
                    @csrf 
                    <div class="modal-body">
                        <div class="form-group"> 
                            <textarea name="dess"   class="form-control" rows="2"> </textarea> 
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Observar</button>
                    </div>
                    </div>
                    </form>
    </div>
  </div>
</div>
                         <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#Pobs-{{$value->id}}" >Observar</button>
                    </td> 
                    @else
                    <td>Procesado</td>
                    @endif 
                        <?php 
                         echo '</tr>'; 
                        }  

                        ?> 
                    </tbody>
                    </table>
                </div>
                   
                    
                </div>
            </div>
        </div>
    </div>
</div>



@endsection

