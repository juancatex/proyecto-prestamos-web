<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Registro No Disponible</title>
</head>
<body >
<div class="form-control" >
    <h1>Registro no Disponible</h1> 
    @if($respuesta=="nohora")

        Horario de Atencion {{$horainicio}} - {{$horafin}} <br>
        lunes a viernes
    @else
        <h2>Ya Existe una Solicitud en este Mes</h2>
        En Caso de emergencia comunicarse al Whatsapp: 71554528
    @endif
    <a href="http://www.ascinalss.org/ascinalss/index.html">Volver</a>
</div>
</body>
</html>

