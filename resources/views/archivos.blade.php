@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Mis Archivos') }}</div>

                <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">nombre archivo</th>
                            <th scope="col">ver</th>
                            <th scope="col">eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($files as $file)
                        <tr>
                            <th scope="row">{{ $file->id }}</th>
                            <td>{{ $file->nombre }}</td>
                            <td> <a href="storage/pdf/{{ Auth::id() }}/{{ $file->nombre }}" class="btn btn-sm btn-outline-secondary">Ver</a></td>
                            <td> <a href="" class="btn btn-sm btn-outline-danger">Eliminar</a></td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                    </table>
                </div>
                   
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
