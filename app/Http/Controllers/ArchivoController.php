<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Archivo;
use App\Models\registro;
use App\Models\Observacion;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert; 
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
  

class ArchivoController extends Controller
{
    public function index(){
      
        $files= registro::where('estado',1)
                            ->orderby('id','desc')
                                ->get();

        foreach ($files as $value) {
            $archivos=Archivo::where('idregistro',$value->id)
                            ->get();
            $value->archivos=$archivos;
        } 
        return view('prueba',compact('files'));
    }
    
    
    public function store(Request $request)
    {
        $request->validate([
            'documentos.*' => 'required|mimes:png,jpg,jpeg,pdf|max:10240',
            'solicitado' => 'required|integer|gt:100|lte:5000'
            ]);

        $mesactual=date("m");
        $tieneregistro=registro::where('ci',$request->ci)
                                ->whereMonth('created_at',$mesactual)
                                ->where('estado',1)
                                ->get()
                                ->toArray();
        
        if(count($tieneregistro)>0)
        { 
            return back()->withErrors(["your_custom_error"=>"¡Ya tiene un registro de solicitud de prestamo!"]);
        }
        else
        { 
            DB::beginTransaction();
            {
                try{  
                    $paso=0; 
                    if($request->hasFile('documentos'))
                    { 
                        $registro = new registro();
                        $registro->nombres = strtoupper($request->nombres);
                        $registro->ci = $request->ci;
                        $registro->liquido = $request->liquido;
                        $registro->solicitado = $request->solicitado;
                        $registro->plazo = $request->plazo;
                        $registro->cuenta = $request->cuenta;
                        $registro->telefono = $request->telefono;
                        $registro->save();
                        $id=$registro->id;
    
                        $files =$request->file('documentos');  
                        foreach ($files as $file) {
                            if(Storage::putFileAs('/public/pdf/'.$id.'/',$file,$file->getClientOriginalName())){
                                Archivo::create(['nombre'=>$file->getClientOriginalName(), 'idregistro'=>$id]); 
                            }else{
                                $paso=1;
                                break;
                            }
                        } 
                    }
                      
                    if($paso==0){
                        DB::commit();
                        Alert::success('Correcto', 'Registro exitoso'); 
                        return back();
                    }else{
                        throw new ModelNotFoundException("Ocurrio un error al momento de registrar los datos, intentelo nuevamente."); 
                    }
                    
                 }
                catch(\Exception $e)
                { 
                    DB::rollback(); 
                    Alert::error('Error!',$e->getMessage()); 
                    return back();
                }
            }  
        }
 
    }
   
    public function update(Request $request)
            {
                $registro = registro::findOrFail($request->id);
                $registro->estado = '0';
                $registro->user =Auth::id();
                $registro->save();
                Alert::success('Correcto', 'Registro modificado correctamente');   
                return back();
            }
    public function observacion(Request $request)
            {
                $registro = new Observacion();
                $registro->obs =$request->dess;
                $registro->idregistro =$request->id;
                $registro->user =Auth::id();
                $registro->save(); 
                Alert::success('Correcto', 'Registro modificado correctamente');   
                return back();
            }
}
