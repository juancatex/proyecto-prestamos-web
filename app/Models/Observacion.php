<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Observacion extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'observaciones';
    protected $fillable= ['obs','idregistro'];
}
