<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class registro extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'registros';
    protected $fillable = ['nombres','ci','liquido','solicitado','plazo','cuenta','telefono','estado','obs'];

}
