const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .vue()
//     .sass('resources/sass/app.scss', 'public/css');

mix.js('resources/js/app.js', 'public/js')
.vue()   
.styles([
    'resources/css/material-dashboard.css',
    'resources/css/font-awesome.min.css', 
    'resources/css/fonts-googleapis.css'
],  'public/css/plantilla.css') 
.scripts([
    'resources/assets/js/bootstrap-material-design.min.js',
    'resources/assets/js/material-dashboard.js',
    'resources/assets/js/main.js'
], 'public/js/login.js')
.scripts([
    'resources/assets/js/trfdp.js', 
    'resources/assets/js/ttrfdp.js'
], 'public/js/appsia.js');
