<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Auth::check()) { 
      $mes = date("n");
      $activo = 1;
      switch (Auth::user()->tipouser) {
          case 1:
          return view('prestamos',compact('mes','activo'));
          break; 
          case 2:
          return view('prestamos',compact('mes','activo'));
          break; 
          case 3:
          return view('prestamos',compact('mes','activo'));
          break; 
          case 4:
          return view('responsable',compact('mes','activo'));
          break; 
      }
    }
    else  
      return redirect()->route('login');
});
Route::get('/fecha/{id}/vista/{vista}/tipo/{activo}', function ($mes,$vista,$activo) { 
  if(Auth::check()) {
    return view($vista,compact('mes','activo'));
  }else{
    return redirect()->route('login');
  } 
})->name('fecha');

//Auth::routes(); 
Auth::routes(['reset' => false,'register' => false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');  
Route::post('/upload',  [App\Http\Controllers\ArchivoController::class, 'store'])->name('registroapi');
Route::post('/update/{id}',  [App\Http\Controllers\ArchivoController::class, 'update'])->name('update');
Route::post('/observacion/{id}',  [App\Http\Controllers\ArchivoController::class, 'observacion'])->name('observacion'); 
Route::get('/archivos', 'App\Http\Controllers\ArchivoController@index');
Route::get('/registro', function () {
  
      $horainicio="08:30";
      $horafin="16:30";
      $dia = date("l");
      $hora= strtotime(date("H:i:s"));
      
      if($dia!="Saturday" && $dia != "Sunday")
      {
          if($hora>=strtotime($horainicio) && $hora<=strtotime($horafin))
          {
              return view('registro'); 
          }
          else{
              $respuesta="nohora";
              return view('result',compact('horainicio','horafin','respuesta'));
          }  
      }
      else
      {
          $respuesta="nohora";
          return view('result',compact('horainicio','horafin','respuesta'));
      }
});
 
Route::get('/link', function () {
    Artisan::call('storage:link');  
  });